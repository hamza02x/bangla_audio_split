package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	hel "github.com/hamza02x/go-helper"
	"github.com/tcolgate/mp3"
)

const dir = "/Volumes/PUBLIC/Deeni/bangla quran audio/processing_v1/audio"

type xFile struct {
	Name string
	Path string
}

var ayaCount = []int{
	7, 286, 200, 176, 120, 165, 206, 75, 129, 109, 123, 111,
	43, 52, 99, 128, 111, 110, 98, 135, 112, 78, 118, 64, 77,
	227, 93, 88, 69, 60, 34, 30, 73, 54, 45, 83, 182, 88, 75,
	85, 54, 53, 89, 59, 37, 35, 38, 29, 18, 45, 60, 49, 62, 55,
	78, 96, 29, 22, 24, 13, 14, 11, 11, 18, 12, 12, 30, 52, 52,
	44, 28, 28, 20, 56, 40, 31, 50, 40, 46, 42, 29, 19, 36, 25,
	22, 17, 19, 26, 30, 20, 15, 21, 11, 8, 8, 19, 5, 8, 8, 11,
	11, 8, 3, 9, 5, 4, 7, 3, 6, 3, 5, 4, 5, 6}

func main() {

	// printTotalAudioDuration()
	generateMissingDotTxt()
}

func printTotalAudioDuration() {

	var seconds = 0.0

	for sura := 1; sura <= 114; sura++ {

		var suraStr = strconv.Itoa(sura)
		var dirSura = dir + "/" + suraStr

		for _, ayaFile := range getFiles(dirSura) {
			seconds += getAudioLength(ayaFile.Path)
		}
	}

	hel.Pl("Total seconds:", seconds, "i.e:", time.Duration(seconds)*time.Second)
}

func generateMissingDotTxt() {

	var out = ""
	var missingCount = 0

	for sura := 1; sura <= 114; sura++ {

		var suraStr = strconv.Itoa(sura)

		var dirSura = dir + "/" + suraStr

		for aya := 1; aya <= ayaCount[sura-1]; aya++ {
			var fname = zerofy(suraStr) + zerofy(strconv.Itoa(aya)) + ".mp3"
			var fpath = dirSura + "/" + fname
			if !hel.FileExists(fpath) {
				missingCount++
				out += suraStr + " - " + strconv.Itoa(aya) + "\n"
			}
		}

		out += "\n"
	}

	out = strings.ReplaceAll(out, "\n\n\n", "\n")
	out = strings.ReplaceAll(out, "\n\n\n", "\n")

	hel.Pl("missing aya file count -", missingCount)

	if err := hel.StrToFile("missing.txt", out); err != nil {
		panics(err)
	}

}

// 1.mp3 => 001001.mp3
func suraFuncRename(sura int, suraStr string) {

	hel.Pl("processing - sura", sura)

	var dirSura = dir + "/" + suraStr

	for _, ayaFile := range getFiles(dirSura) {

		// 001001.mp3 => len = 10
		if len(ayaFile.Name) == 10 {
			continue
		}

		// renameing from 1.mp3 to 001001.mp3
		// ayaNo, err := strconv.Atoi(strings.ReplaceAll(ayaFile.Name, ".mp3", ""))
		// panics(err)

		// var destFileName = zerofy(suraStr) + zerofy(strconv.Itoa(ayaNo)) + ".mp3"

		// os.Rename(dirSura+"/"+ayaFile.Name, dirSura+"/"+destFileName)
	}

}

func getFiles(dirPath string) []xFile {

	var files []xFile

	err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() && !strings.HasPrefix(info.Name(), ".") {
			files = append(files, xFile{
				Name: info.Name(),
				Path: path,
			})
		}
		return nil
	})

	panics(err)

	return files
}

// 1 	=> 001
// 12 	=> 012
// 114 	=> 114
func zerofy(s string) string {
	if len(s) == 1 {
		return "00" + s
	} else if len(s) == 2 {
		return "0" + s
	}
	return s
}

func getAudioLength(filepath string) float64 {

	t := 0.0

	r, err := os.Open(filepath)
	defer r.Close()

	if err != nil {
		fmt.Println(err)
		return t
	}

	d := mp3.NewDecoder(r)
	var f mp3.Frame
	skipped := 0

	for {

		if err := d.Decode(&f, &skipped); err != nil {
			if err == io.EOF {
				break
			}
			fmt.Println(err)
			return t
		}

		t = t + f.Duration().Seconds()
	}

	return t
}

func panics(err error) {
	if err != nil {
		panic(err)
	}
}
